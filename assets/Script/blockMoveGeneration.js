var tmpShip = require('ship');
cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    setInputControl: function()
    {
        var self = this;
        var listener = 
        {
          event: cc.EventListener.TOUCH_ONE_BY_ONE,
          onTouchBegan: function(touches, event)
          {
              var goAction= cc.moveBy(0.2,cc.p(0,-140));
              self.node.runAction(goAction);
              return true;
          },
          
        }
        cc.eventManager.addListener(listener, self.node);
    },
    
    blockMovement: function()
    {
        var self = this;
        var goAction = cc.moveBy(2, cc.p(0,-140));
        self.node.runAction(goAction);
        return true;
    },
    // use this for initialization
    onLoad: function () {
        this.setInputControl();
        this.blockMovement();
    },
    
    noteBox: function()
    {
      return this.node.getBoundingBoxToWorld();  
    },
    
    update: function(dt)
    {
        var ship = cc.find("Canvas/ship").getComponent(tmpShip);
        if(cc.rectIntersectsRect(ship.node.getBoundingBoxToWorld(),this.noteBox()))
        {
            cc.director.loadScene("GameOver");
        }
        
    },
    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
