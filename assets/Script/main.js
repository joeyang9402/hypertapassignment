cc.Class({
    extends: cc.Component,

    properties: 
    {
      // the main role of this game
      ship:
      {
          default:null,
          type:cc.Node
      },
      
      //the blocks
      block:
      {
          default:null,
          type:cc.Prefab
      },
      
      // the amount of blocks
      blockCount:0,
      
       // score label
       scoreLabel:
       {
           default:null,
           type:cc.Label
       },
       
       //score value
       score:0,
       
       // the distance between two blocks
       blockDistance:140,

    },

    //the operation of the ship moving from left to right
    shipMoveLeft:function()
    {
        var goLeft= cc.moveTo(0.2,cc.p(-this.node.width/2+130,this.ship.getPositionY()));
        this.ship.runAction(goLeft);
        
    },
    
    
    
    //the operation of the ship moving from right to left
    shipMoveRight:function()
    {
        var goRight= cc.moveTo(0.2,cc.p(this.node.width/2-130,this.ship.getPositionY()));
        this.ship.runAction(goRight);
        
    },
    
    //get a new block
    NewBlock:function()
    {
        this.blockCount+=1;
        var newBlock = cc.instantiate(this.block);
        this.node.addChild(newBlock);
        var rand= cc.random0To1();
        cc.log(rand);
        //set the position of new block
        newBlock.setPosition(this.blockPosition(rand));
        
      
    },
    
    // set the position of new block
    blockPosition:function(rand)
    {
        var randX=0;
        var randY=0;
        //if rand > 0.5, then the new block appears in the right position, otherwise, left
        if(rand>=0.5)
        {
            randX=this.node.width/2-130;
            cc.log(randX);
        }
        else
        {
            randX=-this.node.width/2+130;
            cc.log(randX);  
        }
        if(this.blockCount<=6)
        {
            randY=(-this.node.height/2)+(this.blockDistance*this.blockCount)+this.blockDistance*1;
        }
        else
        {
            randY=(-this.node.height/2)+(this.blockDistance*6)+this.blockDistance*1;
        }
        return cc.p(randX,randY);
    },
    
    //handling the player input
    setInputControl:function()
    {
        var self = this;
        var listener = 
        {
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            onTouchBegan: function (touches, event) 
            {
                //get the target
                var target = event.getCurrentTarget();
                
                //determine the ship shipDestination
                var shipDestination = target.convertToNodeSpace(touches.getLocation());
                cc.log(shipDestination);
                if(shipDestination.x > self.node.width/2)
                {
                   self.shipMoveRight();//moving to the right
                }
                else
                {
                   self.shipMoveLeft();//moving to the left
                }
                
                
                self.NewBlock();
                return true; 
            },
            onTouchMoved: function (touches, event) 
            {
            },
            onTouchEnded: function (touches, event) 
            {
           
            },
            onTouchCancelled: function (touches, event) 
            {
            }
        }
        cc.eventManager.addListener(listener, self.node);
    },

    // use this for initialization
    onLoad: function () 
    {
       
       this.setInputControl();
       this.ship.setPosition(-this.node.width/2+130,-this.node.height/2+70);
        for(var i=0;i<6;i++)
        {
            cc.log("111");
            this.NewBlock();
        }

       
    },

    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});